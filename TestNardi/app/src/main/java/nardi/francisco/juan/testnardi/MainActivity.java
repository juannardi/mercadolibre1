package nardi.francisco.juan.testnardi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;


public class MainActivity extends AppCompatActivity {

    //Variables
    private static final String TAG = "MainActivity";
    Adapter_Productos adapter = null;
    ListView listView;
    SearchView searchView;
    Intent intent;
    String query;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("mercado libre");
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setLogo(R.mipmap.ic_launcher_ml);
        intent = getIntent();
        listView = (ListView) findViewById(R.id.lista_productos);
        query = null;
        //En caso de llamada a la actividad principal donde se muestran los productos desde la
        // actividad de exploración por categorías, se pasa el id_categoraa que viene en el intent de la llamada al adaptador del listview
        if(intent.getExtras() != null) {
                populateListView( "category=" + intent.getStringExtra("id_categoria"));
            }else{
            if(savedInstanceState != null && savedInstanceState.containsKey("query")){
                String a = savedInstanceState.getString("query");
                query = a;
                populateListView("q=" + savedInstanceState.getString("query"));
            }else {
                populateListView("");
            }
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_inicio, menu);
        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem search_in_bar = menu.findItem(R.id.action_search);
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT){
            //Si la orientación de la pantalla es portrait, oculto el cuadro de búsqueda de la actionbar
            search_in_bar.setVisible(false);
            //instancio el cuadro de búsqueda por defecto de la actividad
            searchView = (SearchView) findViewById(R.id.searchviewv);
            //onclicklistener
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    //llamo al adaptador con el query
                    populateListView("q=" + query);
                    //oculto el teclado
                    hideKeyboard(MainActivity.this);
                    return true;
                }
                @Override
                public boolean onQueryTextChange(String newText) {
                    //autocomplete, en caso que se requiera
                    return true;
                }
            });
        }
        else
        {
            search_in_bar.setVisible(true);
            //instancio el cuadro de búsqueda de la actionbar
            searchView = (SearchView) search_in_bar.getActionView();
            searchView.setQueryHint(getString(R.string.buscar_en_mercadolibre));
            searchView.setBackgroundColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                searchView.setBackground(getDrawable(R.drawable.bg_white_rounded));
            }
            //quitar el cuadro de búsqueda por defecto del layout
            SearchView searchViewv = (SearchView) findViewById(R.id.searchviewv);
            searchViewv.setVisibility(View.GONE);
            //onclicklistener
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    populateListView("q="+query);
                    hideKeyboard(MainActivity.this);
                    return true;
                }
                @Override
                public boolean onQueryTextChange(String newText) {
                    //autocomplete
                    return true;
                }
            });
        }
        if(query != null && !query.isEmpty())
            searchView.setQuery(query, false);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.compartir:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Código de evaluación. Candidato: Nardi Juan Francisco" +
                        System.getProperty("line.separator") + "Has probado esta App? .Apk disponible en " + getResources().getResourceName(R.string.link_repo));

                startActivity(Intent.createChooser(shareIntent, "Has probado esta App? .Apk disponible en " +  getResources().getResourceName(R.string.link_repo)));
                return true;
            case R.id.salir:
                this.finish();
                return true;
            case R.id.contactarse:
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getResources().getResourceName(R.string.email)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Test App ML");
                startActivity(Intent.createChooser(emailIntent, "Envía un e-mail..."));
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void populateListView(String tags){

        adapter = new Adapter_Productos(MainActivity.this, tags);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Item_Producto item_producto = (Item_Producto) adapter.getItem(position);

                if(item_producto != null) {
                    //Al hacer click en un producto, me dirijo a la actividad Detalles, enviándole la clase item_producto completa en un bundr
                    Intent goDetalle = new Intent(getApplicationContext(), DetalleActivity.class);
                    goDetalle.putExtra("item_producto", item_producto);
                    startActivity(goDetalle);
                }
            }

        });
        // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


    }

    public void go_categorias(View view){
        Intent goCategorias = new Intent(getApplicationContext(), CategoriasActivity.class);
        startActivity(goCategorias);
           }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("position", listView.getFirstVisiblePosition()); // get current recycle view position here.
        try {
            if (searchView.getQuery().length() > 0)
                savedInstanceState.putString("query", searchView.getQuery().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        try {

            final int position = savedInstanceState.getInt("position");

            listView.clearFocus();
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(position);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error en cambio de scroll " + e.getMessage());
        }


    }
    @Override
    public void onPause() {
        super.onPause();
        adapter.limpiar_dialogos();
    }



}
