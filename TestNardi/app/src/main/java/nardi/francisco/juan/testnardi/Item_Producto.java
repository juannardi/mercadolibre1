package nardi.francisco.juan.testnardi;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Item_Producto implements Serializable {// Comparable<Item_Producto> {

    private String id;
    private String site_id;
    private String title;
    private String seller_id;
    private String seller_trans_total;
    private String seller_trans_canceled;
    private String seller_ratings_negative;
    private String seller_ratings_positive;
    private String seller_ratings_neutral;
    private String seller_trans_completed;
    private String seller_status;
    private String price;
    private String available_quantity;
    private String sold_quantity;
    private String buying_mode;
    private String stop_time;
    private String condition;
    private String permalink;
    private String thumbnail;
    private String accepts_mercadopago;
    private String installments_quantity;
    private String installments_amount;
    private String installments_rate;
    private String installments_currency_id;
    private String address_state_name;
    private String address_city_name;
    private boolean shipping_free;
    private String shipping_mode;
    private boolean shipping_store_pick_up;
    private ArrayList<Atributos_Producto> atributos;
    private String original_price;
    private String category_id;
    private String official_store_id;
    private String domain_id;
    private String catalog_product_id;
    private String catalog_listing;

    public Item_Producto(String id, String site_id, String title, String seller_id, String seller_trans_total, String seller_trans_canceled, String seller_ratings_negative, String seller_ratings_positive, String seller_ratings_neutral, String seller_trans_completed, String seller_status, String price, String available_quantity, String sold_quantity, String buying_mode, String stop_time, String condition, String permalink, String thumbnail, String accepts_mercadopago, String installments_quantity, String installments_amount, String installments_rate, String installments_currency_id, String address_state_name, String address_city_name, boolean shipping_free, String shipping_mode, boolean shipping_store_pick_up, ArrayList<Atributos_Producto> atributos, String original_price, String category_id, String official_store_id, String domain_id, String catalog_product_id, String catalog_listing) {
        this.id = id;
        this.site_id = site_id;
        this.title = title;
        this.seller_id = seller_id;
        this.seller_trans_total = seller_trans_total;
        this.seller_trans_canceled = seller_trans_canceled;
        this.seller_ratings_negative = seller_ratings_negative;
        this.seller_ratings_positive = seller_ratings_positive;
        this.seller_ratings_neutral = seller_ratings_neutral;
        this.seller_trans_completed = seller_trans_completed;
        this.seller_status = seller_status;
        this.price = price;
        this.available_quantity = available_quantity;
        this.sold_quantity = sold_quantity;
        this.buying_mode = buying_mode;
        this.stop_time = stop_time;
        this.condition = condition;
        this.permalink = permalink;
        this.thumbnail = thumbnail;
        this.accepts_mercadopago = accepts_mercadopago;
        this.installments_quantity = installments_quantity;
        this.installments_amount = installments_amount;
        this.installments_rate = installments_rate;
        this.installments_currency_id = installments_currency_id;
        this.address_state_name = address_state_name;
        this.address_city_name = address_city_name;
        this.shipping_free = shipping_free;
        this.shipping_mode = shipping_mode;
        this.shipping_store_pick_up = shipping_store_pick_up;
        this.atributos = atributos;
        this.original_price = original_price;
        this.category_id = category_id;
        this.official_store_id = official_store_id;
        this.domain_id = domain_id;
        this.catalog_product_id = catalog_product_id;
        this.catalog_listing = catalog_listing;
    }

    public String getId() {
        return id;
    }

    public String getSite_id() {
        return site_id;
    }

    public String getTitle() {
        return title;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public String getSeller_trans_total() {
        return seller_trans_total;
    }

    public String getSeller_trans_canceled() {
        return seller_trans_canceled;
    }

    public String getSeller_ratings_negative() {
        return seller_ratings_negative;
    }

    public String getSeller_ratings_positive() {
        return seller_ratings_positive;
    }

    public String getSeller_ratings_neutral() {
        return seller_ratings_neutral;
    }

    public String getSeller_trans_completed() {
        return seller_trans_completed;
    }

    public String getSeller_status() {
        return seller_status;
    }

    public String getPrice() {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);

        return format.format(Float.parseFloat(price)).replace(",", " ").replace(".", ",").replace(" ", ".").replace(",00", "").replace("$", "");

    }

    public String getAvailable_quantity() {
        return available_quantity;
    }

    public String getSold_quantity() {
        return sold_quantity;
    }

    public String getBuying_mode() {
        return buying_mode;
    }

    public String getStop_time() {
        return stop_time;
    }

    public String getCondition() {
        return condition;
    }

    public String getPermalink() {
        return permalink;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getAccepts_mercadopago() {
        return accepts_mercadopago;
    }

    public String getInstallments_quantity() {
        return installments_quantity;
    }

    public String getInstallments_amount() {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);

        return format.format(Float.parseFloat(installments_amount)).replace(",", " ").replace(".", ",").replace(" ", ".").replace(",00", "").replace("$", "");


    }

    public String getInstallments_rate() {
        return installments_rate;
    }

    public String getInstallments_currency_id() {
        return installments_currency_id;
    }

    public String getAddress_state_name() {
        return address_state_name;
    }

    public String getAddress_city_name() {
        return address_city_name;
    }

    public boolean isShipping_free() {
        return shipping_free;
    }

    public String getShipping_mode() {
        return shipping_mode;
    }

    public boolean isShipping_store_pick_up() {
        return shipping_store_pick_up;
    }

    public ArrayList<Atributos_Producto> getAtributos() {
        return atributos;
    }

    public String getOriginal_price() {
        return original_price;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getOfficial_store_id() {
        return official_store_id;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public String getCatalog_product_id() {
        return catalog_product_id;
    }

    public String getCatalog_listing() {
        return catalog_listing;
    }

  /*  @Override
    public int compareTo(Item_Producto item_producto) {
        if ( item_producto.shipping_free) {
            return 1;
        }
        else{
            return -1;
        }

    } */


}
