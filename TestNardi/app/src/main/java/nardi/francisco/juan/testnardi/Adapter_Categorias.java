package nardi.francisco.juan.testnardi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Adapter_Categorias extends ArrayAdapter {

    private RequestQueue requestQueue;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static final String TAG = "AdapterCategorias";
    List<Item_Categoria> items;

    private String URL_BASE = "https://api.mercadolibre.com/sites/MLA/categories";
    private ProgressDialog progressDialog;


    public Adapter_Categorias(Context context, String tags) {
        super(context, 0);

        requestQueue = Volley.newRequestQueue(context);
        mRequestQueue = Volley.newRequestQueue(context);
        items = new ArrayList<Item_Categoria>();
        //Voy a usar un imageloader para cargar las imágenes de manera asíncrona y no demore la visualización de la vista
        //puede ser útil en entornos con pocos recursos o conexión deficiente, también produce un parpadeo en la precarga
        // que puede resultar molesto..
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Cargando..");
        progressDialog.setMessage("Buscando Categorías...");
        progressDialog.show();

        if (tags.isEmpty()) {
            //es el caso de la pantalla inicial, no hay categoría padre

            JsonArrayRequest JsonObjectRequestRegistro;
            final String finalTags = tags;
            JsonObjectRequestRegistro = new JsonArrayRequest(
                    Request.Method.GET,
                    URL_BASE + tags, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {

                            parseJson(response);

                            if ((progressDialog != null) && progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if ((progressDialog != null) && progressDialog.isShowing())
                                progressDialog.dismiss();
                            Toast.makeText(getContext(), "No se encuentra disponible en este momento...", Toast.LENGTH_LONG).show();
                        }
                    }
            );
            requestQueue.add(JsonObjectRequestRegistro);

        } else {
            obtener_categoria_id(tags);
        }
    }


    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public Item_Categoria getItem(int position) {
        return items.get(position);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItemView = layoutInflater.inflate(R.layout.item_categoria, null);

        final Item_Categoria item = items.get(position);
        TextView titulo = (TextView) listItemView.findViewById(R.id.mensaje_titulo);

        titulo.setText(item.getName());

        String foto = item.getPicture();

        if (!foto.equals("null") && !foto.equals("")) {

            NetworkImageView detalle_foto = (NetworkImageView) listItemView.findViewById(R.id.mensaje_foto);
            detalle_foto.setImageUrl(foto, mImageLoader);
            detalle_foto.setDefaultImageResId(R.mipmap.ic_launcher_ml_foreground);
            detalle_foto.setErrorImageResId(R.drawable.error);
        }
        return listItemView;
    }

    public void parseJson(JSONArray jsonObject) {

        if (!jsonObject.isNull(0)) {

            for (int i = 0; i < jsonObject.length(); i++) {
                try {
                    JSONObject objeto_respuesta = jsonObject.getJSONObject(i);
                    //busco la información de la categoía, id por id
                    obtener_categoria(objeto_respuesta.getString("id"));

                } catch (JSONException e) {
                    Log.e(TAG, "Error de parsing: " + e.getMessage());
                    Toast.makeText(getContext(), "Error en la consulta, inténtelo mas tarde ", Toast.LENGTH_LONG).show();
                }
            }

        }
        if (jsonObject.isNull(0)) {//lo repite cada vez que el volley reconsulta
            Toast.makeText(getContext(), "Error accediendo a la información, inténtelo mas tarde", Toast.LENGTH_LONG).show();

        }
        // Collections.sort(posts);
        //  return posts;
    }

    public void parseJsonCategoria(JSONObject jsonObject) {

        if (jsonObject.has("id")) {
            try {
                //las guardo en mi clase  Item_categoria
                Item_Categoria item_categoria = new Item_Categoria(
                        jsonObject.getString("id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("picture"),
                        jsonObject.getString("permalink"));
                items.add(item_categoria);
                notifyDataSetChanged();

            } catch (Exception e) {
                Log.e(TAG, "Error de parsing: " + e.getMessage());
                Toast.makeText(getContext(), "Error en la consulta, inténtelo mas tarde ", Toast.LENGTH_LONG).show();
            }

        } else {//no puede obtener el item de la categoría
            Toast.makeText(getContext(), "Error accediendo a la información, inténtelo mas tarde", Toast.LENGTH_LONG).show();
        }
    }

    public void obtener_categoria(String id) {


        JsonObjectRequest JsonObjectRequestRegistro;
        JsonObjectRequestRegistro = new JsonObjectRequest(
                Request.Method.GET,
                "https://api.mercadolibre.com/categories/" + id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsonCategoria(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if ((progressDialog != null) && progressDialog.isShowing())
                            progressDialog.dismiss();
                        Toast.makeText(getContext(), "No se encuentra disponible en este momento...", Toast.LENGTH_LONG).show();
                    }
                }
        );
        requestQueue.add(JsonObjectRequestRegistro);
    }

    public void obtener_categoria_id(final String id) {
        //Para el caso de categorías hijas

        final JsonObjectRequest JsonObjectRequestRegistro;
        JsonObjectRequestRegistro = new JsonObjectRequest(
                Request.Method.GET,
                "https://api.mercadolibre.com/categories/" + id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        limpiar_dialogos();

                        JSONArray hijos = null;
                        try {
                            hijos = response.getJSONArray("children_categories");
                            if (hijos.length() == 0) {

                                Intent goMain = new Intent(getContext(), MainActivity.class);
                                goMain.putExtra("id_categoria", id);
                                getContext().startActivity(goMain);
                            } else {

                                for (int i = 0; i < hijos.length(); i++) {

                                    JSONObject objeto_respuesta = hijos.getJSONObject(i);
                                    obtener_categoria(objeto_respuesta.getString("id"));
                                }
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            Log.e(TAG, "Error de parsing: " + e.getMessage());
                            Toast.makeText(getContext(), "Error en la consulta, inténtelo mas tarde ", Toast.LENGTH_LONG).show();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if ((progressDialog != null) && progressDialog.isShowing())
                            progressDialog.dismiss();
                        Toast.makeText(getContext(), "No se encuentra disponible en este momento...", Toast.LENGTH_LONG).show();
                    }
                }
        );
        requestQueue.add(JsonObjectRequestRegistro);
    }


    public void limpiar_dialogos(){
        if ((progressDialog != null) && progressDialog.isShowing())
            progressDialog.dismiss();
    }


}

