package nardi.francisco.juan.testnardi;

public class Item_Categoria {
    private String id;
    private String name;
    private String picture;
    private String permalink;
    /*
    private String total_items_in_this_category;
    private String path_from_root;
    private String children_categories;
    private String attribute_types;
    private String settings;
    private String meta_categ_id;
    private String attributable;
    private String date_created;
    */

    public Item_Categoria(String id, String name, String picture, String permalink) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.permalink = permalink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }
}
