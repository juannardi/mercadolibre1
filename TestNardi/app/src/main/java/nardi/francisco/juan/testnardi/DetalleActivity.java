package nardi.francisco.juan.testnardi;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.GatheringByteChannel;
import java.util.ArrayList;

public class DetalleActivity extends AppCompatActivity {

    Item_Producto item_producto;
    private RequestQueue requestQueue;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private String URL_BASE = "https://api.mercadolibre.com/items/";//MLA842370689/description
    private static final String TAG = "DetalleActivity";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private float x1, x2 = 0f;
    static final int MIN_DISTANCE = 50;
    static final int click_DISTANCE = 5;
    NetworkImageView imagen_principal;
    String[] direcciones;
    int indice = 0;
    boolean imagen_maximizada = false;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("mercado libre");
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setLogo(R.mipmap.ic_launcher_ml);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            item_producto = (Item_Producto) getIntent().getSerializableExtra("item_producto");

            ((TextView) findViewById(R.id.condicion_producto)).setText(item_producto.getCondition().equals("new") ? "Producto Nuevo" : "Producto Usado");
            ((TextView) findViewById(R.id.rating_producto)).setText(item_producto.getSeller_status().equals("null") ? "" : "Status del Vendedor: " + item_producto.getSeller_status());

            ((TextView) findViewById(R.id.titulo_producto)).setText(item_producto.getTitle());
            ((TextView) findViewById(R.id.precio_producto)).setText("$ " + item_producto.getPrice());
            ((TextView) findViewById(R.id.stock_producto)).setText("Stock Disponible (" + item_producto.getAvailable_quantity() + ")");
            if (!item_producto.isShipping_free()) {
                ((Button) findViewById(R.id.producto_envio_normalidad)).setVisibility(View.GONE);
            }
            if (!item_producto.getOriginal_price().equals("null")) {
                ((TextView) findViewById(R.id.precio_original)).setText("Precio Anterior: $ " + item_producto.getOriginal_price());
            }

            if (item_producto.getAccepts_mercadopago().equals("false")) {
                ((TextView) findViewById(R.id.acepta_mercado)).setVisibility(View.INVISIBLE);
                ((ImageView) findViewById(R.id.iv_mp)).setVisibility(View.INVISIBLE);
            }
            //--------------------------------
            if (item_producto.getInstallments_quantity().equals("")) {
                ((LinearLayout) findViewById(R.id.lv_cuotas)).setVisibility(View.GONE);
            } else {
                ((TextView) findViewById(R.id.tv_cuotas)).setText(item_producto.getInstallments_quantity() + " Cuotas de $ " + item_producto.getInstallments_amount());
            }
            String envio = "";
            if (item_producto.isShipping_free())
                envio = "Envío Gratis";
            if (item_producto.isShipping_store_pick_up())
                envio += "+ Puedo Recogerlo de la Tienda ";
            // if(!item_producto.getShipping_mode().equals("not_specified"))
            //   envio += item_producto.getShipping_mode();
            ((TextView) findViewById(R.id.tv_envio)).setText(envio);
            if (item_producto.getOfficial_store_id().equals("null")) {
                ((LinearLayout) findViewById(R.id.lv_tienda)).setVisibility(View.GONE);
            }
            int total = Integer.valueOf(item_producto.getSeller_trans_total());

            SeekBar totalSeekBar = (SeekBar) findViewById(R.id.seekBar_total);
            totalSeekBar.setMax(total);
            totalSeekBar.setProgress(total);
            totalSeekBar.setEnabled(false);
            ((TextView) findViewById(R.id.seekBartotaltv)).setText("Transacciones Totales: " + total);

            SeekBar positivasSeekBar = (SeekBar) findViewById(R.id.seekBar_positivas);
            positivasSeekBar.setMax(100);
            float p = Float.parseFloat(item_producto.getSeller_ratings_positive());
            positivasSeekBar.setProgress(Math.round(100 * p));
            positivasSeekBar.setEnabled(false);
            ((TextView) findViewById(R.id.seekBarpositivastv)).setText("Transacciones Positivas: " + Math.round(100 * p));

            SeekBar negativasSeekBar = (SeekBar) findViewById(R.id.seekBar_negativas);
            negativasSeekBar.setMax(100);
            p = Float.parseFloat(item_producto.getSeller_ratings_negative());
            negativasSeekBar.setProgress(Math.round(100 * p));
            negativasSeekBar.setEnabled(false);
            ((TextView) findViewById(R.id.seekBarnegativastv)).setText("Transacciones Negativas: " + Math.round(100 * p));

            SeekBar neutrasSeekBar = (SeekBar) findViewById(R.id.seekBar_neutral);
            neutrasSeekBar.setMax(100);
            p = Float.parseFloat(item_producto.getSeller_ratings_neutral());
            neutrasSeekBar.setProgress(Math.round(100 * p));
            neutrasSeekBar.setEnabled(false);
            ((TextView) findViewById(R.id.seekBarneutraltv)).setText("Transacciones Neutrales: " + Math.round(100 * p));

            SeekBar canceladasSeekBar = (SeekBar) findViewById(R.id.seekBar_canceladas);
            canceladasSeekBar.setMax(100);
            p = Float.parseFloat(item_producto.getSeller_trans_canceled());
            canceladasSeekBar.setProgress(Math.round(100 * p));
            canceladasSeekBar.setEnabled(false);
            ((TextView) findViewById(R.id.seekBarcanceladastv)).setText("Transacciones Canceladas: " + Math.round(100 * p));


            SeekBar completasSeekBar = (SeekBar) findViewById(R.id.seekBar_completas);
            completasSeekBar.setMax(100);
            p = Float.parseFloat(item_producto.getSeller_trans_completed());
            completasSeekBar.setProgress(Math.round(100 * p));
            completasSeekBar.setEnabled(false);
            ((TextView) findViewById(R.id.seekBarcompletastv)).setText("Transacciones Completadas: " + Math.round(100 * p));
//------------------------------- tables -----------
            TableLayout table = (TableLayout) findViewById(R.id.table_caracteristicas);
            ArrayList<Atributos_Producto> atributos_productoList = item_producto.getAtributos();
            for (int i = 0; i < atributos_productoList.size(); i++) {
                Atributos_Producto atributo = atributos_productoList.get(i);
                TableRow tableRow2 = new TableRow(DetalleActivity.this);
                TextView tvid = new TextView(DetalleActivity.this);

                tvid.setText(atributo.getName() + " : " + atributo.getValue_name());
                tableRow2.addView(tvid);
                table.addView(tableRow2);
            }

            //busco la descripción general del producto
            buscar_datos();
            //Busco las imágenes de portada
            buscar_imagenes();
            imagen_principal = (NetworkImageView) findViewById(R.id.producto_imagen_principal);
            imagen_principal.setDefaultImageResId(R.mipmap.ic_launcher_ml_foreground);

        } else {
            onBackPressed();
        }
        // verifico el tipo de interacción sobre la imagen, en caso de ser un swap de derecha a izquierda, muestro la siguiente imagen
        // si es un click, la maximizo
        imagen_principal.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                int eid = event.getAction();
                switch (eid) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        return true;
                    case MotionEvent.ACTION_BUTTON_RELEASE:
                        x1 = event.getX();
                        return true;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        float deltaX = x2 - x1;
                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            if (x2 < x1 && indice < (direcciones.length - 1))
                                mostrar_portada(++indice);

                            if (x2 > x1 && indice >= 1)
                                mostrar_portada(--indice);
                        } else {
                            ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) imagen_principal.getLayoutParams();
                            if (imagen_maximizada) {
                                params.height = (int) (150 * getResources().getDisplayMetrics().density);
                                ;
                                imagen_principal.setLayoutParams(params);
                                imagen_maximizada = false;
                            } else {

                                if (Math.abs(deltaX) < click_DISTANCE) {
                                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

                                        params.height = Resources.getSystem().getDisplayMetrics().heightPixels - 200;
                                        imagen_principal.setLayoutParams(params);
                                        imagen_maximizada = true;
                                    } else {

                                        params.height = Resources.getSystem().getDisplayMetrics().widthPixels - 200;
                                        imagen_principal.setLayoutParams(params);
                                        imagen_maximizada = true;
                                    }
                                }

                            }

                        }


                        break;
                    case MotionEvent.ACTION_CANCEL:
                        x2 = event.getX();
                        float deltaX_c = x2 - x1;
                        if (Math.abs(deltaX_c) > MIN_DISTANCE) {
                            if (x2 < x1 && indice < (direcciones.length - 1))
                                mostrar_portada(++indice);

                            if (x2 > x1 && indice >= 1)
                                mostrar_portada(--indice);
                        }


                        break;

                    default:
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sin_busqueda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.compartir:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Ya viste este producto en mercado libre? " +
                        System.getProperty("line.separator") + item_producto.getPermalink());

                startActivity(Intent.createChooser(shareIntent, "Ya viste este producto en mercado libre? " + item_producto.getPermalink()));
                return true;
            case R.id.salir:
                this.finish();
                return true;
            case R.id.contactarse:
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getResources().getResourceName(R.string.email)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Test App ML");
                startActivity(Intent.createChooser(emailIntent, "Envía un e-mail..."));
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void buscar_datos() {
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest JsonObjectRequestProductos;
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        //Voy a usar un imageloader para cargar las imágenes de manera asíncrona y no demore la visualización de la vista
        //puede ser útil en entornos con pocos recursos o conexión deficiente, también produce un parpadeo en la precarga
        // que puede resultar molesto..
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });

        JsonObjectRequestProductos = new JsonObjectRequest(
                Request.Method.GET,
                URL_BASE + item_producto.getId() + "/description/", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        procesarRespuesta_descripcion(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(JsonObjectRequestProductos);
    }


    protected void procesarRespuesta_descripcion(JSONObject jsObject) {
        try {

            ((TextView) findViewById(R.id.tvdescripcion)).setText(jsObject.getString("plain_text"));

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Error obteniendo descripcion" + e.getMessage());
        }

    }

    private void buscar_imagenes() {
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest JsonObjectRequestProductos;

        JsonObjectRequestProductos = new JsonObjectRequest(
                Request.Method.GET,
                URL_BASE + item_producto.getId(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        procesarRespuesta_imagenes(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(JsonObjectRequestProductos);
    }


    protected void procesarRespuesta_imagenes(JSONObject jsObject) {
        try {

            JSONArray arreglo_respuesta = jsObject.getJSONArray("pictures");
            if (arreglo_respuesta.length() == 0) {
                Toast.makeText(this, "No encontramos imágenes para este producto...", Toast.LENGTH_LONG).show();
            }
            direcciones = new String[arreglo_respuesta.length()];
            for (int i = 0; i < arreglo_respuesta.length(); i++) {
                try {
                    JSONObject jsonObject = arreglo_respuesta.getJSONObject(i);
                    String urlw;
                    if (jsonObject.has("secure_url")) {
                        urlw = jsonObject.getString("secure_url");
                    } else {
                        urlw = jsonObject.getString("url");
                    }
                    direcciones[i] = urlw;


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Error obteniendo imagenes" + e.getMessage());
                }
            }
            if (arreglo_respuesta.length() > 0) {
                indice = 0;
                mostrar_portada(0);
            }


            //adaptador_galería(direcciones);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void mostrar_portada(int i) {

        imagen_principal.setImageUrl(direcciones[i], mImageLoader);

    }


}
