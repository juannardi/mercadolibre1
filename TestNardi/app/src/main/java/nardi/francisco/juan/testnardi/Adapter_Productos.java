package nardi.francisco.juan.testnardi;

import android.app.ProgressDialog;
import android.content.Context;

import android.graphics.Bitmap;

import android.os.Build;

import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

import java.util.List;


public class Adapter_Productos extends ArrayAdapter {

    private RequestQueue requestQueue;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static final String TAG = "AdapterProductos";
    List<Item_Producto> items;
    private String URL_BASE = "https://api.mercadolibre.com/sites/MLA/search?";

    private final ProgressDialog progressDialog;


    public Adapter_Productos(Context context, String tags) {
        super(context, 0);

        requestQueue = Volley.newRequestQueue(context);
        mRequestQueue = Volley.newRequestQueue(context);
        items = new ArrayList<Item_Producto>();
        //Voy a usar un imageloader para cargar las imágenes de manera asíncrona y no demore la visualización de la vista
        //puede ser útil en entornos con pocos recursos o conexión deficiente, también produce un parpadeo en la precarga
        // que puede resultar molesto..
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Cargando..");
        progressDialog.setMessage("Buscando Productos...");
        progressDialog.show();

        //A los fines de este examen, y por no contar con datos de interacciones previas del cliente, se va a presentar por
        //defecto una búsqueda de "Celulares". Así no aparece la pantalla vacía
        if (tags.isEmpty() || tags.equals("category=null")) {
            tags = "q=celulares";
        }

        JsonObjectRequest JsonObjectRequestProductos;

        JsonObjectRequestProductos = new JsonObjectRequest(
                Request.Method.GET,
                URL_BASE + tags, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseJson(response);
                        notifyDataSetChanged();
                        if ((progressDialog != null) && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if ((progressDialog != null) && progressDialog.isShowing())
                            progressDialog.dismiss();
                        Toast.makeText(getContext(), "No se encuentra disponible en este momento...", Toast.LENGTH_LONG).show();
                    }
                }
        );
        requestQueue.add(JsonObjectRequestProductos);
    }


    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public Item_Producto getItem(int position) {
        return items.get(position);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItemView = layoutInflater.inflate(R.layout.item_producto, null);

        final Item_Producto item = items.get(position);

        if (!item.isShipping_free()) { //cambiar por el envío con normalidad
            Button envio_normalidad = (Button) listItemView.findViewById(R.id.producto_envio_normalidad);
            envio_normalidad.setVisibility(View.INVISIBLE);
        }

        TextView titulo = (TextView) listItemView.findViewById(R.id.producto_titulo);
        TextView precio = (TextView) listItemView.findViewById(R.id.producto_precio);
        TextView extras = (TextView) listItemView.findViewById(R.id.producto_extras);

        titulo.setText(item.getTitle());
        precio.setText("$ " + item.getPrice());
        String extras_msj = "";
        if (item.isShipping_free()) {
            extras_msj = "Envío Gratis";
        }
        if (item.isShipping_store_pick_up()) {
            extras_msj += "  Puede retirar en Tienda";
        }
        extras.setText(extras_msj);

        String foto = item.getThumbnail(); //consultar por galería

        if (!foto.equals("null") && !foto.equals("")) {

            NetworkImageView producto_foto = (NetworkImageView) listItemView.findViewById(R.id.producto_foto);
            producto_foto.setImageUrl(foto, mImageLoader);
            producto_foto.setDefaultImageResId(R.mipmap.ic_launcher_ml_foreground);
            producto_foto.setErrorImageResId(R.drawable.error);

        }
        return listItemView;
    }

    public void parseJson(JSONObject jsonObject_response) {

        try {
            if (jsonObject_response.has("results")) {
                JSONArray arreglo_respuesta = jsonObject_response.getJSONArray("results");
                if (arreglo_respuesta.length() == 0) {
                    Toast.makeText(getContext(), "No encontramos productos con esa descripción...", Toast.LENGTH_LONG).show();
                }
                for (int i = 0; i < arreglo_respuesta.length(); i++) {
                    try {
                        JSONObject jsonObject = arreglo_respuesta.getJSONObject(i);
                        JSONObject reputacion;
                        if (jsonObject.getJSONObject("seller").has("seller_reputation")) {
                            reputacion = jsonObject.getJSONObject("seller").getJSONObject("seller_reputation");
                        } else {
                            reputacion = null;
                        }
                        //En esta instancia, me encontré con la decisión de cargar solo lo que iba a mostrar en la listview
                        //o cargar el contenido d ela consulta completo para ser reutilizado en la pantalla de detalles de producto
                        // opté por realizar la carga aquí, no es la solución mas elegante.
                        try {


                            Item_Producto item_producto = new Item_Producto(
                                    jsonObject.getString("id"), jsonObject.getString("site_id"),
                                    jsonObject.getString("title"), jsonObject.getJSONObject("seller").getString("id"),
                                    reputacion != null ? reputacion.getJSONObject("transactions").getString("total") : "",
                                    reputacion != null ? reputacion.getJSONObject("transactions").getString("canceled") : "",
                                    reputacion != null ? reputacion.getJSONObject("transactions").getJSONObject("ratings").getString("negative") : "",
                                    reputacion != null ? reputacion.getJSONObject("transactions").getJSONObject("ratings").getString("positive") : "",
                                    reputacion != null ? reputacion.getJSONObject("transactions").getJSONObject("ratings").getString("neutral") : "",
                                    reputacion != null ? reputacion.getJSONObject("transactions").getString("completed") : "",
                                    reputacion != null ? reputacion.getString("power_seller_status") : "",
                                    jsonObject.getString("price"),
                                    jsonObject.getString("available_quantity"), jsonObject.getString("sold_quantity"),
                                    jsonObject.getString("buying_mode"), jsonObject.getString("stop_time"),
                                    jsonObject.getString("condition"), jsonObject.getString("permalink"),
                                    jsonObject.getString("thumbnail"),
                                    jsonObject.getString("accepts_mercadopago"),
                                    jsonObject.getString("installments").equals("null") ? "" : jsonObject.getJSONObject("installments").getString("quantity"),
                                    jsonObject.getString("installments").equals("null") ? "" : jsonObject.getJSONObject("installments").getString("amount"),
                                    jsonObject.getString("installments").equals("null") ? "" : jsonObject.getJSONObject("installments").getString("rate"),
                                    jsonObject.getString("installments").equals("null") ? "" : jsonObject.getJSONObject("installments").getString("currency_id"),
                                    jsonObject.getJSONObject("address").getString("state_name"), jsonObject.getJSONObject("address").getString("city_name"),
                                    jsonObject.getJSONObject("shipping").getBoolean("free_shipping"),
                                    jsonObject.getJSONObject("shipping").getString("mode"),
                                    jsonObject.getJSONObject("shipping").getBoolean("store_pick_up"),
                                    jsonObject.has("attributes") ? obtener_atributos(jsonObject.getJSONArray("attributes")) : null,
                                    jsonObject.has("original_price") ? jsonObject.getString("original_price") : "",
                                    jsonObject.has("category_id") ? jsonObject.getString("category_id") : "",
                                    jsonObject.has("official_store_id") ? jsonObject.getString("official_store_id") : "",
                                    jsonObject.getString("domain_id"),
                                    jsonObject.has("catalog_product_id") ? jsonObject.getString("catalog_product_id") : "",
                                    jsonObject.has("catalog_listing") ? jsonObject.getString("catalog_listing") : "");

                            items.add(item_producto);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "Error de parsing dato " + e.getMessage());
                        }
                        //  notifyDataSetChanged();
                    } catch (JSONException e) {
                        Log.e(TAG, "Error de parsing array " + e.getMessage());
                        // Toast.makeText(getContext(), "Error en la consulta, inténtelo mas tarde ", Toast.LENGTH_LONG).show();
                    }
                }
         /*   if(switch_envio){
                Collections.sort(items);
            } */

            } else {
                Toast.makeText(getContext(), "Sin productos para su búsqueda", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //En el caso de los atributos, cree una clase a parte y voy a pasar el arreglo a la clase Detalle de Producto cuando se seleccione uno
    private ArrayList<Atributos_Producto> obtener_atributos(JSONArray atributos_arraay) {
        if (!atributos_arraay.isNull(0)) {
            try {
                ArrayList<Atributos_Producto> atributos = new ArrayList<Atributos_Producto>();
                for (int i = 0; i < atributos_arraay.length(); i++) {
                    try {
                        JSONObject objeto_respuesta = atributos_arraay.getJSONObject(i);

                        Atributos_Producto Post = new Atributos_Producto(
                                objeto_respuesta.getString("id"),
                                objeto_respuesta.getString("name"),
                                objeto_respuesta.getString("value_name"));

                        atributos.add(Post);
                    } catch (JSONException e) {
                        Log.e(TAG, "Error de parsing: " + e.getMessage());
                        Toast.makeText(getContext(), "Error en la consulta, inténtelo mas tarde ", Toast.LENGTH_LONG).show();
                    }
                }
                return atributos;
            } catch (Exception e) {
                Log.e(TAG, "Error obteniendo atributos: " + e.getMessage());
                Toast.makeText(getContext(), "Error en la consulta, inténtelo mas tarde ", Toast.LENGTH_LONG).show();
            }
        }
        return null;
    }

    public void limpiar_dialogos() {
        if ((progressDialog != null) && progressDialog.isShowing())
            progressDialog.dismiss();
    }


}

