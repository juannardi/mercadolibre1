package nardi.francisco.juan.testnardi;

import java.io.Serializable;

class Atributos_Producto implements Serializable{

    private String id;
    private String name;
    private String value_name;

    /* A los fines de este examen, se descarta el dato "values": [], y otros ...
    private String value_struct;
    private String attribute_group_name;
    private String source;
    private String value_id;
    private String attribute_group_id;
    */


    public Atributos_Producto(String id, String name, String value_name) {
        this.id = id;
        this.name = name;
        this.value_name = value_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue_name() {
        return value_name;
    }

    public void setValue_name(String value_name) {
        this.value_name = value_name;
    }
}
